package ru.itpark.pets.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.pets.domain.Dog;
import ru.itpark.pets.repository.DogsRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;


@Service
public class DogsService {
    private final DogsRepository dogsRepository;


    public DogsService(DogsRepository dogsRepository) {
        this.dogsRepository = dogsRepository;
    }

    public List<Dog> findAll() {
        return dogsRepository.findAll();
    }

    public Dog getById(int id) {
        return dogsRepository.findById(id);
    }


    public List<Dog> findByName(String name) {
        return dogsRepository.findByName(name);
    }

    public String save(MultipartFile picture) throws IOException {
        String name = UUID.randomUUID().toString() + ".jpg";
        Path path = Paths.get("Pets\\src\\main\\resources\\files\\images"); // FIXME: тут, лучше, конечно, путь полный прописать
        Path target = path.resolve(Paths.get(name));

        picture.transferTo(target.toFile());

        return name;
    }

    public void delete(String pictureName) throws IOException {
        Path path = Paths.get("Pets\\src\\main\\resources\\files\\images"); // FIXME: тут, лучше, конечно, путь полный прописать
        Path target = path.resolve(Paths.get(pictureName));

        Files.deleteIfExists(target);
    }

    public void removeById(int id) throws IOException {
       String pictureUrl = dogsRepository.findById(id).getPictureUrl();

       delete(pictureUrl);

       dogsRepository.removeById(id);
    }

    public void add(Dog dog, MultipartFile picture) throws IOException {
        String pictureUrl = save(picture);
        dog.setPictureUrl(pictureUrl);
        dogsRepository.add(dog);
    }
}

