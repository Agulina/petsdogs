package ru.itpark.pets.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.image.BufferedImage;

@Data
@AllArgsConstructor
public class Dog {
    private int id;
    private String name;
    private int price;
    private String pictureUrl;
    private String content;

    public String getPictureUrl() {
        return this.pictureUrl;
    }
    public void setPictureUrl(String url) {
        this.pictureUrl = url;
    }
}
