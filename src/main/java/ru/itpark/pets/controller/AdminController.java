package ru.itpark.pets.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itpark.pets.domain.Dog;
import ru.itpark.pets.service.DogsService;


import java.io.IOException;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final DogsService dogsService;

    public AdminController(DogsService dogsService) {
        this.dogsService = dogsService;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("dogs", dogsService.findAll());

        return "admin/admin";
    }

    @GetMapping("/search")
    public String findByName(
        @RequestParam String name,
        Model model
    ) {
        model.addAttribute("search", name);
        model.addAttribute("dogs", dogsService.findByName(name));

        return "dogs";
    }

    @GetMapping("/{id}")
    public String getById(
            @PathVariable int id,
            Model model
    ) {
        model.addAttribute("dog", dogsService.getById(id));

        return "admin/view";
    }

    @PostMapping("/{id}/remove")
    public String remove(@PathVariable int id) throws IOException {
        dogsService.removeById(id);

        return "redirect:/admin";
    }

    @GetMapping("/add")
    public String addDog() {
        return "admin/add";
    }

    @PostMapping("/add")
    public String add(@ModelAttribute Dog dog, @RequestPart MultipartFile picture) throws IOException {
        dogsService.add(dog, picture);
        return "redirect:/admin";
    }
}
