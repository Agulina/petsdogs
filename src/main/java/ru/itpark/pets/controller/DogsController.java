package ru.itpark.pets.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itpark.pets.service.DogsService;


@Controller
@RequestMapping("/dogs")
public class DogsController {
    private final DogsService dogsService;

    public DogsController(DogsService dogsService) {
        this.dogsService = dogsService;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("dogs", dogsService.findAll());

        return "dogs";
    }

    @GetMapping("/search")
    public String findByName(
            @RequestParam String name,
            Model model
    ) {
        model.addAttribute("search", name);
        model.addAttribute("dogs", dogsService.findByName(name));

        return "dogs";
    }

    @GetMapping("/{id}")
    public String getById(
            @PathVariable int id,
            Model model
    ) {
        model.addAttribute("dog", dogsService.getById(id));

        return "dog";
    }
}


